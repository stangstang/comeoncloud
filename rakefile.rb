require 'rake\testtask'

Rake::TestTask.new('test') do |t|
	t.test_files=FileList['test/test*.rb']
	t.warning=true
end

desc "build the bridge"
task :build_bridge do
	puts "build bridge is on"
end

desc "Cross the bridge"
task :cross_bridge=>[:build_bridge] do
	puts "cross_bridge is default"
end

task:default=>[:test]
