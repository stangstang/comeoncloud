require 'block_file'
require 'block'
require 'test/unit'
class SelfStockTest <Test::Unit::TestCase
	
	def setup
		@self_stock=Block.class_of("Dzh").new  
		@self_stock.backup
	end

	def test_have
		@self_stock.clean
		@self_stock.add "SH600875"
		@self_stock.add "SZ000001"
		assert_equal  @self_stock.have,["SH600875","SZ000001"]
	end
	
	def test_add_batch
		stocks=["SH600001","SZ000001","SH600875","SZ000002"]
 		old_stocks=@self_stock.have
		puts "old stock in add array = ",old_stocks
 		@self_stock.add_batch stocks 
 		assert_equal @self_stock.have, (old_stocks+stocks).sort.uniq
	end
	
	def test_del
		@self_stock.clean
		@self_stock.add_batch ["SH600001","SZ000001","SH600875","SZ000002"] 
		@self_stock.del "SH600001" 
		assert_equal @self_stock.have, ["SH600875","SZ000001","SZ000002"] 
	end

	def teardown
		@self_stock.restore
	end
end

class SelfStockFileTest <Test::Unit::TestCase

	def setup
		@self_stock_file=DzhBlockFile.new 
		@self_stock_file.backup
	end

	def test_display
		puts @self_stock_file.display
	end
	
	def test_clean_zero
		@self_stock_file.clean_zero
		assert_equal "\245\000Q\377",@self_stock_file.read
	end
	
	def test_codes
		@self_stock_file.clean_zero
		@self_stock_file.write "SH600875"
		@self_stock_file.write "SZ000001"
		assert_equal  @self_stock_file.codes,["SH600875","SZ000001"]
	end

	def test_backup
		@self_stock_file.backup
		assert_equal @self_stock_file.read , @self_stock_file.read_backup
	end

	def test_restore
		@self_stock_file.backup
		@self_stock_file.restore
		assert_equal @self_stock_file.read , @self_stock_file.read_backup
	end
	
	def test_del
		@self_stock_file.clean_zero
		@self_stock_file.write_array_of ["SH600001","SZ000001","SH600875","SZ000002"] 
		@self_stock_file.del "SH600001" 
		assert_equal @self_stock_file.codes, ["SH600875","SZ000001","SZ000002"] 
	end
 
 	def test_write_array_of
 		stocks=["SH600001","SZ000001","SH600875","SZ000002"]
 		old_stocks=@self_stock_file.codes
		puts "old stock in  write arraye= ",old_stocks
 		@self_stock_file.write_array_of	stocks 
 		assert_equal @self_stock_file.codes, (old_stocks+stocks).sort.uniq
 	end

	def teardown
		@self_stock_file.restore 
	end 

end

