require "block_file"

module Block

	extend self		
	
	def class_of(software)
		case software
			when "Hx"	
				create_block_class(HxBlockFile)
			when "Dzh"
				create_block_class(DzhBlockFile)
			end
			
	end

	private
	def create_block_class(software_file)

		Class.new(software_file) do

			def initialize
				super
			end

			def clean
				clean_zero
			end

			def	have	
				codes
			end

			def add(stock)
				add_batch [stock]
			end
			
			def add_batch(stocks)
				write_array_of  stocks
			end

			def del(stock)
				super stock
			end
		end
	end

end
