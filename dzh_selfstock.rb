module DzhFileProperty

	def	initialize()
			@file="d:\\ProgramFiles\\DZH\\userdata\\block\\��ѡ��.BLK"
			@start="\245\000Q\377"
			@internal="\000\000\000\000"
			@backup_file=@file+".tmp"
			@code_regular_expression=/S[H|Z]\d{6}/
	end 
	
	def after_read_each(stock)
		return stock
	end

	def before_yield_each(stock)
		return stock
	end

end


module DzhSelfStockFile

	include DzhFileProperty

	def	initialize
		super	
	end 

	def display
		open(@file,'rb').each { |x| p x}
	end

	def backup
		open(@backup_file,'wb') {|f| f<< read}
	end
	
	def restore
		open(@file,'wb') {|f| f<< read_backup}
	end

	def	clean_zero	
		puts "@file = nil"  if (@file==nil)  
		open(@file,'wb') {|f| f<< @start}
	end

	def read
		IO.read(@file)
	end

	def read_backup
		IO.read(@backup_file)
	end

	def	codes
		@codes=[]
		read.scan(@code_regular_expression).each do |w|
			@codes<<(after_read_each w)
		end
		@codes.sort.uniq
	end

	def write(stock)
		write_array_of [stock]
	end

	def write_array_of(stocks)
		add_stocks_string=yield_all_string  stocks
		open(@file,'wb') {|f| f << add_stocks_string;f.flush}
	end

	def del(stock)
		stockArray=codes.delete_if {|x| x==stock}
		open(@file,'wb'){|f| f << (yield_string stockArray)}
	end
	
	def yield_all_string(stocks)
		yield_string  (codes+stocks).sort.uniq
	end

	def yield_string(stocks)
		stocks_string=@start
		stocks.sort.uniq.each do |stock| 
			stocks_string+=(before_yield_each(stock)+@internal)  	
		end
	end

	private  :yield_all_string,:yield_string
end 

class DzhBlock 
	
	include DzhSelfStockFile

	def initialize
		super	
	end

	def clean
		clean_zero
	end

	def	have	
		codes
	end

	def add(stock)
		add_batch [stock]
	end
	
	def add_batch(stocks)
		write_array_of  stocks
	end

	def del(stock)
		super stock
	end

end



