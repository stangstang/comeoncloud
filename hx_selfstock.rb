module HxFileProperty

	def	initialize
		@file="D:\\ProgramFiles\\hxzq\\T0002\\blocknew\\ZXG.blk"
		@start=""
		@internal="\r\n"
		@backup_file=@file+".tmp"
		@code_regular_expression=/\d{7}/
	end 

	def after_read_each(stock)
		return stock="SH"+stock[1,7] if stock[0,1]=="1"
 		return stock="SZ"+stock[1,7] if stock[0,1]=="0"	
	end

	def before_yield_each(stock)
		return 	stock="1"+stock[2,6]  if stock[0,2]=="SH"
		return  stock="0"+stock[2,6]  if stock[0,2]=="SZ"
	end

end

module HxSelfstockFile

	include HxFileProperty

	def	initialize
		super
	end 

	def display
		open(@file,'rb').each { |x| p x}
	end

	def backup
		open(@backup_file,'wb') {|f| f<< read}
	end
	
	def restore
		open(@file,'wb') {|f| f<< read_backup}
	end

	def	clean_zero	
		puts "@file = nil"  if (@file==nil)  
		open(@file,'wb') {|f| f<< @start}
	end

	def read
		IO.read(@file)
	end

	def read_backup
		IO.read(@backup_file)
	end

	def	codes
		@codes=[]
		read.scan(@code_regular_expression).each do |w|
 			@codes<< (after_read_each w )
		end
		@codes.sort.uniq
	end


	def write(stock)
		write_array_of [stock]
	end

	def write_array_of(stocks)
		add_stocks_string=yield_all_string  stocks
		open(@file,'wb') {|f| f << add_stocks_string;f.flush}
	end

	def del(stock)
		stockArray=codes.delete_if {|x| x==stock}
		open(@file,'wb'){|f| f << (yield_string stockArray)}
	end
	
	def yield_all_string(stocks)
		yield_string  (codes+stocks).sort.uniq
	end

	def yield_string(stocks)
		stocks_string=@start
		stocks.sort.uniq.each do |stock| 
			stocks_string+=(before_yield_each(stock)+@internal)  	
		end
		stocks_string
	end

	private  :yield_all_string,:yield_string
end 

class HxBlock

	include HxSelfstockFile
	
	def initialize
		super
	end

	def clean
		clean_zero
	end

	def	have	
		codes
	end

	def add(stock)
		add_batch [stock]
	end
	
	def add_batch(stocks)
		write_array_of  stocks
	end

	def del(stock)
		super stock
	end
end

